{ config, pkgs, stdenv, ... }:

with pkgs;
  let
    pkgs-unstable = import <nixos-unstable> {};
  in
{
  imports = [
    ../xwm.nix
    ./kadmos-secret.nix
  ];

  home.packages = [ 
    # anki: spaced repetition flashcard program
    anki

    # keepass: password manager
    keepass

    # dolphin: kde file manager
    dolphin

    # pdf reader with vim keybinds
    zathura
  ];
  
programs.chromium = {
    enable = true;
    extensions = [
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" #uBlock Origin
      "gcbommkclmclpchllfjekcdonpmejbdp" #HTTPS Everywhere

    ];
  };

}
