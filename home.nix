{ config, pkgs, ... }:

  let
    nixpkgs-unstable = import <nixpkgs-unstable> {};
    pvbatchAdditionalPythonPackages = python-packages: with python-packages; [
      numpy
      matplotlib
    ]; 
    pvbatchPython = (pkgs.python2.withPackages pvbatchAdditionalPythonPackages).override { 
      ignoreCollisions = true; 
    }; 
  in
{
  imports = [
    ./vim.nix
    ./active-host.nix
    ./home-secret.nix
  ];


  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = [ 
    pkgs.htop 
    pkgs.ctags
    pkgs.unzip

    # paraview: data analysis and visualization
    # (wrapped for additional python2 packages)
    (pkgs.runCommand "my-paraview" {
      buildInputs = [ pkgs.makeWrapper nixpkgs-unstable.paraview ];
      } ''
          mkdir $out
          ln -s ${nixpkgs-unstable.paraview}/* $out
          rm $out/bin
          mkdir $out/bin
          ln -s ${nixpkgs-unstable.paraview}/bin/* $out/bin
          rm $out/bin/pvbatch
          rm $out/bin/paraview
          rm $out/bin/pvpython
          makeWrapper ${nixpkgs-unstable.paraview}/bin/pvbatch $out/bin/pvbatch-me \
            --set PYTHONPATH "${pvbatchPython}/${pkgs.python.sitePackages}"
          makeWrapper ${nixpkgs-unstable.paraview}/bin/paraview $out/bin/paraview-me \
            --set PYTHONPATH "${pvbatchPython}/${pkgs.python.sitePackages}"
          makeWrapper ${nixpkgs-unstable.paraview}/bin/pvpython $out/bin/pvpython-me \
            --set PYTHONPATH "${pvbatchPython}/${pkgs.python.sitePackages}"
        '')
    pkgs.todoist
    pkgs.fzf
    pkgs.sshfs-fuse
  ];
programs.git = {
    enable = true;
  };

programs.ssh = {
  enable = true;
}; 
programs.bash = {
  enable = true;
  shellAliases = {
    cp = "rsync -ah --inplace --info=progress2";
    gvim = "vim -g";
  };
};
}
