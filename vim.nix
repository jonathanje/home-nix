{ config, pkgs, ... }:

  let
    pkgs-unstable = import <nixos-unstable> {};
  in
{
  home.packages = with pkgs; [ 
    clang-tools 
    gnumake
    gcc
    fzf
    ctags
  ];

programs.vim = { 
    enable = true;
    plugins = with pkgs.vimPlugins; [
      vim #dracula
      vim-gutentags
      fzf-vim
      vim-addon-nix
    ];
    extraConfig = ''
      
      set hidden
      set nobackup
      set nowritebackup
      set signcolumn=yes
      set number
      
      cnoreabbrev <expr> bc ((getcmdtype() is# ':' && getcmdline() is# 'bc')?('bd'):('bc'))
      autocmd VimEnter * colorscheme dracula

      set guioptions-=m "menu bar
      set guioptions-=T "toolbar
      set guioptions-=r "scrollbar
  '';
};
      


}
