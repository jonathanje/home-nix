{ config, lib, pkgs, ... }:

  let
    pkgs-unstable = import <nixos-unstable> {};
  in
{
  home.packages = [ 
    pkgs.i3lock
    pkgs-unstable.i3status-rust
  ];


programs.rofi = {
  enable = true;
  width = 25;
  lines = 3;
  borderWidth = 1;
  padding = 5;
  font = "Source Code Pro 12";
  scrollbar = false;
  separator = "none";
  cycle = true;
  theme = "dracula"; 
};

  xdg.dataFile."rofi/themes/dracula.rasi".text = ''
    // Dracula colors
* {
    background: 	rgba ( 40, 42, 54, 100 % );
    current-line: 	rgba ( 68, 71, 90, 100 % );
    selection: 	rgba ( 68, 71, 90, 100 % );
    foreground: 	rgba ( 248, 248, 242, 100 % );
    comment: 	rgba ( 98, 114, 164, 100 % );
    cyan: 	rgba ( 139, 233, 253, 100 % );
    green: 	rgba ( 80, 250, 123, 100 % );
    orange: 	rgba ( 255, 184, 108, 100 % );
    pink: 	rgba ( 255, 121, 198, 100 % );
    purple: 	rgba ( 189, 147, 249, 100 % );
    red: 	rgba ( 255, 85, 85, 100 % );
    yellow: 	rgba ( 241, 250, 140, 100 % );
}
* {
    selected-normal-background:     @green;
    normal-background:              @background;
    normal-foreground:              @foreground;
    alternate-normal-background:    @normal-background;
    alternate-normal-foreground:    @foreground;
    selected-normal-foreground:     @normal-background;
    urgent-foreground:              @red;
    urgent-background:              @normal-background;
    alternate-urgent-background:    @urgent-background;
    alternate-urgent-foreground:    @urgent-foreground;
    selected-active-foreground:     @foreground;
    selected-urgent-background:     @normal-background;
    alternate-active-background:    @normal-background;
    alternate-active-foreground:    @selected-active-foreground;
    alternate-active-background:    @selected-active-background;
    border-color:                   @selected-normal-background;
    separatorcolor:                 @border-color;
    background-color: @normal-background;
    str: "";
}
#window {
    border:           3;
    padding:          9;
}
#mainbox {
    background-color: inherit;
    border:  0;
    padding: 0;
}
#textbox {
    text-color: @foreground;
}
#element {
    border:  0;
    padding: 1px ;
}
#element.normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
#element.normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
#element.normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
#element.selected.normal {
    background-color: @selected-normal-foreground;
    text-color:       @pink;
}
#element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
#element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
#element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
#element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
#element.alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
#scrollbar {
    border:       0;
}
#button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
#inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    background-color: inherit;
    padding:    1px ;
}
#case-indicator {
    spacing:    0;
    text-color: @normal-background;
}
#entry {
    spacing:    0;
    text-color: @normal-foreground;
}
#prompt {
    spacing:    0;
    text-color: @normal-foreground;
}
    '';

#QWERTZ Layout
home.keyboard.layout = "de";

xdg.configFile."kitty/kitty.conf".text = ''
# https://draculatheme.com/kitty
#
# Installation instructions:
#
#  cp dracula.conf ~/.config/kitty/
#  echo "include dracula.conf" >> ~/.config/kitty/kitty.conf
#
# Then reload kitty for the config to take affect.
# Alternatively copy paste below directly into kitty.conf
foreground #F8F8F2
background #282A36
selection_foreground #282A36
selection_background #F8F8F2
color0     #000000
color8     #4D4D4D
color1     #FF5555
color9     #FF6E67
color2     #50FA7B
color10    #5AF78E
color3     #F1FA8C
color11    #F4F99D
color4     #BD93F9
color12    #CAA9FA
color5     #FF79C6
color13    #FF92D0
color6     #8BE9FD
color14    #9AEDFE
color7     #BFBFBF
color15    #E6E6E6

# URL styles
url_color #BD93F9
url_style single

# Cursor styles
cursor #E6E6E6

# Tab bar colors and styles
tab_fade 1
active_tab_foreground #282A36
active_tab_background #F8F8F2
active_tab_font_style bold
inactive_tab_foreground #F8F8F2
inactive_tab_background #282A36
inactive_tab_font_style normal
'';


xsession = {
  enable = true;
  windowManager.i3 = {
    enable = true;
    package = pkgs.i3-gaps;
    config.modifier = "Mod4";
    config.keybindings = lib.mkOptionDefault {
      "${config.xsession.windowManager.i3.config.modifier}+d" = "exec ${pkgs.rofi}/bin/rofi -show run -display-run '>' -no-show-match";
      "${config.xsession.windowManager.i3.config.modifier}+Return" = "exec ${pkgs.kitty}/bin/kitty";
      "Control+Mod1+l" = "exec ${pkgs.i3lock}/bin/i3lock -i ~/Pictures/constant/i3lock/i3lock.png";
    };
    config.bars = [
      { statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ${config.xdg.configHome}/i3/status.toml"; }
    ];
  };
};
  xdg.configFile."i3/status.toml".text = ''
    [[block]]
    block = "disk_space"
    path = "/"
    alias = "/"
    info_type = "available"
    unit = "GB"
    interval = 20
    warning = 20.0
    alert = 10.0
    
    [[block]]
    block = "memory"
    display_type = "memory"
    format_mem = "{Mup}%"
    format_swap = "{SUp}%"
    
    [[block]]
    block = "cpu"
    interval = 1
    
    [[block]]
    block = "load"
    interval = 1
    format = "{1m}"
    
    [[block]]
    block = "time"
    interval = 60
    format = "%a %d/%m %R"
    '';

services.random-background.enable = true;
services.random-background.imageDirectory = "%h/Pictures/constant/wallpaper/";

}


